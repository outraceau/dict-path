
"""Expose a convenient way to refer to keys in a dictionary

dict_path module exposes a fluid interface that allows for convenient referral to values
hidden deep in large Pyhon dictionaries.

Example:
    Instead of referring to the "PublicIp" key by this error prone, manual expression of hierarchy:
    return ec2_instance_data['Reservations'][0]['Instances'][0]['NetworkInterfaces'][0]['Association']['PublicIp'],
    you would:
    1) Create a path object first:
    path = new DictPath()
    path.step('Reservations') \
        .step(0) \
        .step('Instances) \
        .step(0) \
        .step('NetworkInterfaces') \
        .step(0) \
        .step('Association') \
        .step('PublicIp)

    2) Use the create path object and traverse_dict() function to refer to values in a dictionary
    public_ip = traverse_dict(some_dict, path)

dict_path will tell you explicitely which setp in a path it couldn't find with
an informative error message

ToDo:
1) Make DictPath composable - make it possible to composethe final path out of multiple
smaller path objects
2) Make the path object configurable from the config options for convenient amendements
in case of e.g. API changes
"""

from typing import Iterable, List, Dict

class DictPath():
    """Definition of path for dictionary traversal. an iterable object.

    Each step in the traversal path is added with add_step() method.
    Class is iterable in a FIFO fashion (queue).
    An instance of DictPath can be iterated over infinite number of times.

    Attributes:
        steps: A list of traversal steps to take
    """
    def __init__(self):
        self.steps: List = []
        self.current = 0

    def __iter__(self):
        return self

    def __next__(self):
        try:
            result = self.steps[self.current]
            self.current += 1
        except IndexError:
            self.current = 0
            raise StopIteration

        return result

    def __len__(self):
        return len(self.steps)

    def step(self, step):
        """Add traversal step"""
        self.steps.append(step)

        return self

class NoResultAtStepException(Exception):
    """Thrown one there is no result at a given path in a dictionary."""
    pass

def traverse_dict(dictionary: Dict, path: Iterable[DictPath]):
    """Traverse the dict one step at a time.

    Return the result only if all steps in a path qualify
    """
    for step in path:
        try:
            dictionary = dictionary[step]
        except (IndexError, KeyError):
            raise NoResultAtStepException("Non-existen step: " + str(step))

    return dictionary
