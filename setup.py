from distutils.core import setup

setup(
    name='dict_path',
    version='0.1',
    description='Expose a convenient way to refer to keys in a dictionary',
    author='Łukasz Tarasiewicz',
    author_email='lukasz.tarasiewicz@outrace.com.au',
    url='https://bitbucket.org/outraceau/dict-path',
    py_modules=['dict_path']
)
